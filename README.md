# FKT Data Scraper

## Example usage

1. cd fkt-project/
2. Put trail URL data in a file (one trail URL per line, lines beginning # are ignored):
     cat > trail_urls_april_2022.txt
3. Save old results, if applicable:
     mv results.csv results-april-1-2022.csv
4. Remove old intermediate results:
     cd data/
     git clean -f .
     cd ..
6. Run the data download:
     ./commands.sh download trail_urls_april_2022.txt 
7. Run the downloaded-data parser:
     ./commands.sh process trail_urls_april_2022.txt

That produces results.csv which you can put in e.g. Google Sheets. The format
is like:

    $TRAIL,$TRAIL_LINK,$VARIATION,$CAPTION,$NAME,$NAME_LINK,$GENDER,$DURATION,$DATE

Gender is one of male, female, mixed. Variation is e.g. loop, one-way direction, etc.
Caption is e.g. supported/unsupported/?
