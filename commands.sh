#!/bin/bash

# list of dependencies / debian packages 
# sudo apt-get install tidy libxml-xpath-perl curl

set -x
set -e

#trap read debug

# this just takes its input and wraps in a div tag, so if you have a set of
# tags (as xpath may output) you can wrap them in one parent tag, as xpath
# requires, since xpath won't operate on multiple tags at once
function wrap_in_div() {
    echo "<div>$(cat)</div>"
}

# this takes an fkt route variation "table" and turns it into rows in the output
# csv
function process_tables() {
    # incoming xml document has multiple root tags, wrap it for xpath to process
    echo '<div>' > $1-wrapped
    cat $1 >> $1-wrapped
    echo '</div>' >> $1-wrapped

    table_count=$(xpath -e 'count(/div/div/table)' $1-wrapped)
    for ((i=1 ; i <= $table_count ; i++));
    do
	echo "processing table: $i"
	CAPTION=$(xpath -q -e "/div/div[$i]/table/caption/text()" $1-wrapped)
	echo $CAPTION
	row_count=$(xpath -q -e "count(/div/div[$i]/table//tr)" $1-wrapped)
	for ((j=1 ; j <= $row_count ; j++));
	do
	    echo "processing row: $j"
	    # we rewrite commas as plus signs, as we are outputting csv
	    NAME=$(xpath -q -e "/div/div[$i]/table//tr[$j]/td[contains(@class,'athlete')]//text()" $1-wrapped | tr ',' '+')
	    NAME_LINK=$(xpath -q -e "string(/div/div[$i]/table//tr[$j]/td[contains(@class,'athlete')]/a/@href)" $1-wrapped)
	    DURATION=$(xpath -q -e "/div/div[$i]/table//tr[$j]/td[contains(@class,'duration')]//text()" $1-wrapped | tr -d '\n')
	    DATE=$(xpath -q -e "/div/div[$i]/table//tr[$j]/td[contains(@class,'date')]//text()" $1-wrapped | tr -d '\n')

	    # unscraped fields:
	    # views-field-field-multi-sport
	    # views-field-field-flagged
	    
	    echo $TRAIL,$TRAIL_LINK,$VARIATION,$CAPTION,$NAME,$NAME_LINK,$GENDER,$DURATION,$DATE >> results.csv
	done
    done
}

# this takes an fkt's .html and outputs rows to the output csv
function process_file() {
    # the raw html isn't well formed, so run it through tidy to turn it into valid xml for xpath
    ignore=$(tidy -asxml --quote-nbsp no -q $1 > $1-sanitized || :)

    # scan the UL that contains the variation text descriptions in its <li> children
    variation_list_count=$(xpath -q -e 'count(//ul[contains(@class,"quicktabs-tabs")]//li)' $1-sanitized)
    ignore=$(rm $1-variations || :)
    for ((i=1 ; i <= $variation_list_count ; i++));
    do
	echo $(xpath -q -e "//ul[contains(@class,'quicktabs-tabs')]//li[$i]//text()" $1-sanitized | tr -d '\n') >> $1-variations
    done
    
    variation_count=0
    while read VARIATION; do
	echo "variation: $VARIATION"
	VARIATION=$(echo $VARIATION | tr ',' ':')
	xpath -q -e "//div[contains(@id,'quicktabs-tabpage-route-variations-tabs-$variation_count')]" $1-sanitized > $1-variation
	variation_count=$((variation_count + 1))

	# in a variation, the first gender is in a <span>, whereas subsequent genders are
	# just in text nodes in the soup. the genders can be in any order.
	
	# for each gender see if it's in the opening span, find all following nodes before another gender
	xpath -q -e '//span[contains(text(),"Male")]/following-sibling::*[(count(preceding-sibling::text()[contains(.,"Female")]) + count(preceding-sibling::text()[contains(.,"Mixed")])) = 0]' $1-variation > $1-male-first
	xpath -q -e '//span[contains(text(),"Female")]/following-sibling::*[(count(preceding-sibling::text()[contains(.,"Male")]) + count(preceding-sibling::text()[contains(.,"Mixed")])) = 0]' $1-variation > $1-female-first
	xpath -q -e '//span[contains(text(),"Mixed")]/following-sibling::*[(count(preceding-sibling::text()[contains(.,"Female")]) + count(preceding-sibling::text()[contains(.,"Male")])) = 0]' $1-variation > $1-mixed-first

	# for each gender look for it in a text in soup, find nodes following but before another gender
	xpath -q -e '//span[1]/following-sibling::node()[(count(preceding-sibling::text()[contains(.,"Male")]) > 0)]' $1-variation | wrap_in_div | xpath -q -e '/div/*[(count(preceding-sibling::text()[contains(.,"Mixed")]) = 0) and (count(preceding-sibling::text()[contains(.,"Female")]) = 0)]' > $1-male-second
	xpath -q -e '//span[1]/following-sibling::node()[(count(preceding-sibling::text()[contains(.,"Female")]) > 0)]' $1-variation | wrap_in_div | xpath -q -e '/div/*[(count(preceding-sibling::text()[contains(.,"Mixed")]) = 0) and (count(preceding-sibling::text()[contains(.,"Male")]) = 0)]' > $1-female-second
	xpath -q -e '//span[1]/following-sibling::node()[(count(preceding-sibling::text()[contains(.,"Mixed")]) > 0)]' $1-variation | wrap_in_div | xpath -q -e '/div/*[(count(preceding-sibling::text()[contains(.,"Male")]) = 0) and (count(preceding-sibling::text()[contains(.,"Female")]) = 0)]' > $1-mixed-second

	GENDER=male
	process_tables $1-male-first
	process_tables $1-male-second
	GENDER=female
	process_tables $1-female-first
	process_tables $1-female-second
	GENDER=mixed
	process_tables $1-mixed-first
	process_tables $1-mixed-second
    done < $1-variations
}

function download_link() {
    # these headers are just what I pulled out of FF dev tools, used
    # to mitigate chances download script would be ignored as bot traffic
    curl $1 -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:95.0) Gecko/20100101 Firefox/95.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Connection: keep-alive' -H 'Cookie: Drupal-quicktabs-active-tab-id-route-variations-tabs=0' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: cross-site' -H 'Cache-Control: max-age=0' > fkt_route.html
    
    TRAIL=$(basename $1)

    # throttle requests to be nice to website
    sleep 11
}

# takes a list of trail webpage urls as found in trail_urls.txt and downloads
# each to data/trail-name-here.html
# this is separated from the processing so that I only had to download them once
function download_trail_list() {
    while read TRAIL_LINK; do
	case "$TRAIL_LINK" in \#*) continue ;; esac
	download_link $TRAIL_LINK
	TRAIL=$(basename $TRAIL_LINK)
	mv fkt_route.html data/$TRAIL
    done < $1
}

# takes a list of trail webpage urls and parses the pre-downloaded copies into
# results.csv
function process_trail_list() {
    while read TRAIL_LINK; do
	case "$TRAIL_LINK" in \#*) continue ;; esac
	TRAIL=$(basename $TRAIL_LINK)
	process_file data/$TRAIL
    done < $1
}

if [ -z "$2" ]; then
    TRAIL_FILE=trail_urls.txt
else
    TRAIL_FILE="$2"
fi

case $1 in
    download)
	download_trail_list $TRAIL_FILE
	;;

    download-one)
	download_link $2
	;;

    process)
	process_trail_list $TRAIL_FILE
	;;

    *)
	set +x
	echo 'Usage: ./commands.sh {download|process} [TRAIL_URLS_FILE]'
	echo trail URLs file name is optional, by default reads from trail_urls.txt
	echo Will either download each trail URL to a file in data/ or process the downloaded
	echo files and append the processed results to results.csv
	echo
	echo results.csv format:
	echo '$TRAIL,$TRAIL_LINK,$VARIATION,$CAPTION,$NAME,$NAME_LINK,$GENDER,$DURATION,$DATE'
      	echo 
	echo Example: ./commands.sh download
	echo then: ./commands.sh process
	echo
	echo Remember to rm results.csv if you need to restart processing
esac


# debugging setup code
#TRAIL=test
#TRAIL_LINK=test
#process_file test.html
